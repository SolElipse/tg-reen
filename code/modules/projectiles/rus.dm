
//ENERGY//

//LASERS
/obj/item/weapon/gun/energy/laser
 	r_name = "�������&#255; ��������"
 	accusative_case = "�������� ��������"

/obj/item/weapon/gun/energy/laser/practice
 	r_name = "������������&#255; �������&#255; ��������"
 	accusative_case = "������������� �������� ��������"

/obj/item/weapon/gun/energy/laser/captain
 	icon_state = "caplaser"
 	r_name = "����������� �����"

/obj/item/weapon/gun/energy/lasercannon
 	r_name = "�������&#255; �����"
 	accusative_case = "�������� �����"

/obj/item/weapon/gun/energy/xray
 	r_name = "X-ray ��������"
 	accusative_case = "X-ray ��������"

/obj/item/weapon/gun/energy/laser/bluetag
 	r_name = "���������� �����"

/obj/item/weapon/gun/energy/laser/redtag
 	r_name = "���������� �����"



//NUCLEAR//

/obj/item/weapon/gun/energy/gun
 	r_name = "�������������� ��������"

/obj/item/weapon/gun/energy/taser
	r_name = "�����"

/obj/item/weapon/gun/energy/gun/advtaser
	r_name = "�����"

/obj/item/weapon/gun/energy/stunrevolver
	r_name = "����-���������"

/obj/item/weapon/gun/energy/gun/hos/luger
 	r_name = "�������������� �����"

/obj/item/weapon/gun/energy/gun/nuclear
 	r_name = "���������&#255; �������������&#255; ��������"
 	accusative_case = "���������� �������������� ��������"

/obj/item/weapon/gun/energy/gun/dragnet
	r_name = "�������� DRAGnet"

//PULSE
/obj/item/weapon/gun/energy/pulse
 	r_name = "���������&#255; ��������"
 	accusative_case = "���������� ��������"

/obj/item/weapon/gun/energy/pulse/carbine
	r_name = "���������� �������"

/obj/item/weapon/gun/energy/pulse/pistol
	r_name = "���������� ��������"

/obj/item/weapon/gun/energy/pulse/destroyer
	r_name = "���������� �����������"


//SPECIAL

/obj/item/weapon/gun/energy/ionrifle
 	r_name = "������ �����"

/obj/item/weapon/gun/energy/decloner
 	r_name = "������������� ���������&#255;�������"

/obj/item/weapon/gun/energy/floragun
 	r_name = "������� ����������� �����"

/obj/item/weapon/gun/energy/kinetic_accelerator
	r_name = "������������ ����������"

/obj/item/weapon/gun/energy/kinetic_accelerator/crossbow
 	r_name = "�������������� �������"

/obj/item/weapon/gun/energy/plasmacutter
 	r_name = "���������� �����"

/obj/item/weapon/gun/energy/meteorgun
	r_name = "���������"





//PROJECTILE

//AUTOMATIC
/obj/item/weapon/gun/projectile/automatic/c20r
	name = "syndicate SMG"
	r_name = "�������"

/obj/item/weapon/gun/projectile/automatic/l6_saw
	name = "syndicate LMG"
	r_name = "������"

/obj/item/weapon/gun/projectile/automatic/m90
 	name = "syndicate carbine"
 	r_name = "��������� �������"


/obj/item/weapon/gun/projectile/automatic/tommygun
 	name = "tommy gun"
 	r_name = "�����-���"


/obj/item/weapon/gun/projectile/automatic/ar
 	r_name = "��������&#255; ��������"
 	accusative_case = "��������� ��������"

/obj/item/weapon/gun/projectile/automatic/speargun
	r_name = "������������ �������"

//LAUNCHERS
/obj/item/weapon/gun/projectile/revolver/grenadelauncher
	r_name = "���������"

/obj/item/weapon/gun/projectile/automatic/gyropistol
	r_name = "�������� ��������"

/obj/item/weapon/gun/projectile/automatic/speargun
	r_name = "������������ �������"


//REVOLVER

/obj/item/weapon/gun/projectile/revolver
	r_name = "���������"



//SHOTGUN

/obj/item/weapon/gun/projectile/shotgun
	r_name = "��������"

/obj/item/weapon/gun/projectile/revolver/doublebarrel
	r_name = "������������ �����"

/obj/item/weapon/gun/projectile/revolver/doublebarrel/improvised
 	r_name = "����������� �����"

/obj/item/weapon/gun/projectile/automatic/shotgun/bulldog
	r_name = "�������������� ��������"

/obj/item/weapon/gun/projectile/shotgun/automatic/combat
	r_name = "������ ��������"

/obj/item/weapon/gun/projectile/shotgun/boltaction
	r_name = "�������� ������"
	accusative_case = "�������� ������"




//TOY
/obj/item/weapon/gun/projectile/automatic/toy
	r_name = "���������� ��������-������"

/obj/item/weapon/gun/projectile/automatic/toy/pistol
	r_name = "���������� ��������"

/obj/item/weapon/gun/projectile/shotgun/toy
	r_name = "���������� ��������"

/obj/item/weapon/gun/projectile/automatic/c20r/toy
	r_name = "���������� �������"